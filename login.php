<?php session_start();

error_reporting(0);

$logeado = '';
$erreroes = '';
$enviado = '';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $usuario = filter_var(strtolower($_POST['usuario']), FILTER_SANITIZE_STRING);
    $passwd = $_POST['passwd'];
    $passwd = hash('sha512', $_POST['passwd']);

    require 'conexion.php';

    if(!empty($usuario) and !empty($passwd)){
        
        $statement = $conexion -> prepare('SELECT * FROM usuarios WHERE nombre = ? AND passwd = ?');
        $statement -> bind_param('ss', $usuario, $passwd);
        $statement -> execute();

        $result = $statement -> fetch();
        print_r($result);
        if($result === true){
            $_SESSION['usuario'] = $usuario;
            //print_r($_SESSION['usuario']);
            header('Location: index.php');
        }
    } else{
        $erreroes .= 'ingresa tus datos';
    }
}

require 'views/inicio.view.php';
?>
