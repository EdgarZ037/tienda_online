<?php session_start();

error_reporting(0);
if(!isset($_SESSION['usuario'])){
    header('Location: registros.php');
}

require 'conexion.php';
    $statement = $conexion -> query('SELECT * FROM productos WHERE id ='.$_GET['id']);		

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $captcha = $_POST['captcha'];
    
    if($captcha === 123)
        echo 'correcto';
}

?>


<!DOCTYPE html>

<html lang="es">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximu-scale=1, minimun-scale=1">
        <link rel="stylesheet" href="css/estilos.css">  
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">                 
    </head>   
    <body>
      
        <button id="btn"><span class="icon-menu"></span></button>
      
        <header> 
           <nav class="nav2">
               <ul class="menu1">
                <li class="menu">CalvinKlein</li>
                <li class="menu unicode"><a href="login.php.">Inicio</a></li>
                <l1  class="menu menuDad"><a href="#!">Caballeros <span class="icon-down-dir"></span></a>
                 <ul class="menu-child">
                     <li class="menu-item"><a href="paginas/caVestir.php" class="menu-link">Vestir</a></li>
                     <li class="menu-item"><a href="paginas/caInter.php" class="menu-link">Interior</a></li>
                     <li class="menu-item"><a href="paginas/cabAcce.php" class="menu-link">Accesorios</a></li>
                     <li class="menu-item"><a href="paginas/caZapatos.php" class="menu-link">Zapatos</a></li>
                 </ul>
                </l1>
                <l1 class="menu menuDad"><a href="#!">Damas <span class="icon-down-dir"></span></a>
               
                 <ul  class="menu-child">
                     <li class="menu-item"><a href="paginas/DaVestir.php" class="menu-link">Vestir</a></li>
                     <li class="menu-item"><a href="paginas/DaInterior.php" class="menu-link">Interior</a></li>
                     <li class="menu-item"><a href="paginas/DaAccesorio.php" class="menu-link">Accesorios</a></li>
                     <li class="menu-item"><a href="paginas/DaZapatos.php" class="menu-link">Zapatos</a></li>
                 </ul>
                </l1>
                <li class="menu"><a href="cerrar.php">Salir</a></li>
                <li class="menu menu-rigth menuDad" ><a href="#!" ><?php echo $_SESSION['usuario']?><span class="icon-basket"></span></a>                
               </ul>            
           </nav> 
           <hr class="border">

       </header>
        <h1 class="h1">Detalles del Producto</h1>
        
		<?php while($rows = $statement -> fetch_assoc()):?>
       
        <section class="foto"> 
		    <figure class="contenedor3">
		        <img src="productos/<?php echo $rows['imajen'];?>" alt="">
		    </figure>
         
             <a href="carrito.php?id=<?php echo $rows['id'];?>">
                 <button class="carrito icon-basket">añadir</button>
             </a>     
		
        </section>
		<aside class="descripcion">
		   <center><P><?php echo $rows['descripcion']; ?></P></center>
		   <center><p><?php echo '$ '.$rows['precio']; ?></p></center> 
		</aside>

                <form class="icon-compra" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                    <input type="hidden" name="cmd" value="_ext-enter">
                    <input type="hidden" name="redirect_cmd" value="_xclick">
                    <input type="hidden" name="business" value="edgar.figueroa.z037@outlook.com">
                    <input type="hidden" name="currency_code" value="MXN">
                    <input type="hidden" name="first_name" value="">
                    <input type="hidden" name="last_name" value="">
                    <input type="hidden" name="item_name" value="<?php echo $rows['titulo']; ?>">
                    <input type="hidden" name="amount" value="<?php echo $rows['precio']; ?>">
                    <input type="hidden" name="quantity" value="1">

                    <input type="image" src="https://www.paypalobjects.com/webstatic/es_MX/mktg/logos-buttons/redesign/btn_9.png" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
                </form>

        <?php endwhile;?>
        
        <script src="js/jquery.js"></script>
        <script src="js/responsive.js"></script>
    </body>
</html>