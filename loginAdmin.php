<?php session_start();

error_reporting(0);

$errores = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$usuario = filter_var($_POST['usuario'], FILTER_SANITIZE_STRING);
	$password = $_POST['password'];

	if ($password === 'admin123' && $usuario === 'Administrador') {
		$_SESSION['Admin'] = $usuario;
		header('Location: subir.php');
	} else {
		$errores .= '<li>Datos Incorrectos</li>';
	}
}

require 'views/login.view.php'; 

?>