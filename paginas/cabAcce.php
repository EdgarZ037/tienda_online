<?php

require'../conexion.php';
$statement = $conexion -> query("SELECT * FROM productos WHERE prenda LIKE '%Caballero accesorios%'");

?>

<!DOCTYPE html>

<html lang="es">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximu-scale=1, minimun-scale=1">
        <link rel="stylesheet" href="../css/estilos.css">
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/fontello.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>
    <body>
           <button id="btn"><span class="icon-menu"></span></button>

       <header>
           <nav class="nav">
               <ul class="menu1">
                <li class="menu">CalvinKlein</li>
                <li class="menu unicode"><a href="../login.php">Inicio</a></li>
                <l1  class="menu menuDad"><a href="#!">Caballeros <span class="icon-down-dir"></span></a>
                 <ul class="menu-child">
                     <li class="menu-item"><a href="CaVestir.php" class="menu-link">Vestir</a></li>
                     <li class="menu-item"><a href="caInter.php" class="menu-link">Interior</a></li>
                     <li class="menu-item"><a href="cabAcce.php" class="menu-link">Accesorios</a></li>
                     <li class="menu-item"><a href="caZapatos.php" class="menu-link">Zapatos</a></li>
                 </ul>
                </l1>
                <l1 class="menu menuDad"><a href="#!">Damas <span class="icon-down-dir"></span></a>

                 <ul  class="menu-child">
                     <li class="menu-item"><a href="DaVestir.php" class="menu-link">Vestir</a></li>
                     <li class="menu-item"><a href="DaInterior.php" class="menu-link">Interior</a></li>
                     <li class="menu-item"><a href="DaAccesorio.php" class="menu-link">Accesorios</a></li>
                     <li class="menu-item"><a href="DaZapatos.php" class="menu-link">Zapatos</a></li>
                 </ul>
                </l1>

               </ul>
           </nav>
           <hr class="border">

       </header>


       <section class="fotos">
		<div class="contenedor3">
		<?php while($rows = $statement -> fetch_assoc()):?>
			<div class="thumb" id="distant">
                <a href="../comprar.php?id=<?php echo $rows['id']; ?>"><img src="../productos/<?php echo $rows['imajen'];?>" alt="">
				    <div class="description"><p><?php echo $rows['descripcion']?></p>
                       <small>
                         <i class="fa fa-shopping-cart fa-2x copy">
                         <p>Comprar</p>
                         </i>
                       </small>
                    </div>
				</a>
            </div>
       <?php endwhile;?>
       </div>
      </section>

	<footer>
         <div class="sociles">
             <a href="https://www.facebook.com/calvinkleinmexico/?ref=ts&fref=ts" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
             <a href="https://www.youtube.com/user/calvinklein" target="_blank"><i class="fa fa-youtube fa-2x"></i></a>
             <a href="https://twitter.com/calvinklein?lang=es" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
         </div>
          <small>
             <i class="fa fa-copyright fa-2x copy"></i>
              <p>@curiel todos los derechos reservados 2016</p>
          </small>
        </footer>

        <script src="../js/jquery.min.js"></script>
        <script src="../js/responsive.min.js"></script>
    </body>
</html>
