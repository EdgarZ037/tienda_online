<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no,
	 initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	 <link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
	 <link rel="stylesheet" href="css/cssForm.css">
	<title>Cargar</title>
</head>
<body>
    <header> 
	    <nav class="nav">
	        <ul class="menu">
	            <li class="menu-child"><a href="cerrar.php">Cerrar secíon</a></li>
	        </ul>
	    </nav>
    </header> 
    
		<div class="contenedor">
			<h1 class="titulo">Producto</h1>
			<p>descripcion y precio del producto</p>
	
		<form class="formulario" method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
			<label for="producto">Selecciona el produto</label>
			<input type="file" id="producto" name="producto">
			
			<label for="prenda">Seleccione el tipo de prenda</label>
			<select name="prenda" id="prenda">
			    <option>Caballero accesorios</option>
			    <option>Caballero interior</option>
			    <option>Caballero vestir</option>
			    <option>Caballero calzado</option>
			    <option>Damas accesorios</option>
			    <option>Damas interior</option>
			    <option>Damas vestir</option>
			    <option>Damas calzado</option>
            </select> 
            

			<label for="titulo">Nombre del producto</label>
			<input type="text" id="titulo" name="titulo">
			
			<label for="precio">Precio:</label>
			<input type="text" id="precio" name="precio">

			<label for="descripcion">Descripcion:</label>
			<textarea name="descripcion" id="descripcion" placeholder="Ingresa una descripcion"></textarea>

			<?php if(isset($error)):  ?>
			
			    <p class="error"><?php echo $error; ?></p>
			 <?php elseif(isset($enviado)): ?>
               
               <p class="enviado"><?php echo $enviado?></p>
			       
			 <?php endif ?>   
			
			<input type="submit" class="submit" value="Subir Foto">

		</form>
	</div>

	<footer>
	</footer>
</body>
</html