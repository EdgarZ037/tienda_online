<!DOCTYPE html>

<html lang="es">
    <head>
        <title>Registrate :D</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximu-scale=1, minimun-scale=1">
        <link rel="stylesheet" href="css/formulario-registro.css">
        
    </head>    
    <body>
       <h1>Registro</h1>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" class="Form-User">
            <div class="div">
                <h2>Crea una cuenta</h2>
            </div>
            
            <input class="input media" type="text" name="usuario" placeholder="Usuario:" pattern='.{5,}' title="Ingresa un minimo de 5 caracteres" required>
            <input class="input media" type="email" name="correo" placeholder="Correo:" required autofocus>
            <input class="input" type="password" name="passwd" placeholder="Contraseña:" pattern='.{8,}' title="La contraseña miima es de 8 caracteres" required >
            <input class="input" type="password" name="passwd2" placeholder="Repita la contraseña:" required >    
                        
            <input type="submit" name="submit" value="Registrate" class="btn" >
            
            <?php if(!empty($errores)): ?>
				<div class="error">
					<ul>
						<?php echo $errores; ?>
					</ul>
				</div>
				
            <?php elseif(!empty($enviado)): ?>
                <div class="enviado">
                    <ul>
                        <?php echo $enviado; ?>
                    </ul>
                </div>
			<?php endif; ?>
       
        </form>
        
        <p class="p">
            ¿ Ya tienes cuenta ?
			<a href="login.php">Iniciar Sesión</a>
        </p>
        
    </body>
</html> 