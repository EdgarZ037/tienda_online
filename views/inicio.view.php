<!DOCTYPE html>

<html lang="es">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximu-scale=1, minimun-scale=1">
        <link rel="stylesheet" href="css/estilos.css">
        <link rel="stylesheet" href="css/superslides.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>

    <body>

       <header>
           <button id="btn"><span class="icon-menu"></span></button>
           <nav class="nav abrir">
               <ul class="menu1">
                <li class="menu">CalvinKlein</li>
                <li class="menu unicode"><a href="#!">Inicio</a></li>
                <l1  class="menu menuDad"><a href="#!">Caballeros <span class="icon-down-dir"></span></a>
                 <ul class="menu-child ">
                     <li class="menu-item"><a href="paginas/caVestir.php" class="menu-link">Vestir</a></li>
                     <li class="menu-item"><a href="paginas/caInter.php" class="menu-link">Interior</a></li>
                     <li class="menu-item"><a href="paginas/cabAcce.php" class="menu-link">Accesorios</a></li>
                     <li class="menu-item"><a href="paginas/caZapatos.php" class="menu-link">Zapatos</a></li>
                 </ul>
                </l1>
                <l1 class="menu menuDad"><a href="#!">Damas <span class="icon-down-dir"></span></a>

                 <ul  class="menu-child ">
                     <li class="menu-item"><a href="paginas/DaVestir.php" class="menu-link">Vestir</a></li>
                     <li class="menu-item"><a href="paginas/DaInterior.php" class="menu-link">Interior</a></li>
                     <li class="menu-item"><a href="paginas/DaAccesorio.php" class="menu-link">Accesorios</a></li>
                     <li class="menu-item"><a href="paginas/DaZapatos.php" class="menu-link">Zapatos</a></li>
                 </ul>
                </l1>
                <li class="menu">
                    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="get" class="formu">
                        <input type="search" name="buscar" class="icon-search" placeholder="buscar">
                    </form>
                </li>
       <?php if(!isset($_SESSION['usuario'])): ?>
                <li class="menu menu-rigth"><a href='registros.php' target="_blank"><span class="icon-user-add"></span> Sign Up</a></li>
                <li class="menu menu-rigth menuDad" ><a href="#!" id="tirar"><span class="icon-login"></span> Login</a>
                    <form class="form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" name="login">
                        <h6>Ingresa tus datos</h6>
                        <input type="text" class="usuario" name="usuario" id="usuario" placeholder="Usuario" size="20" required>
                        <input type="password" name="passwd" placeholder="Contraseña:" required>
                        <input type="submit" name="submit" class="btn" value="Entrar a CalvinKlein" onclick="login.submit()">
        <?php else: ?>
                        <li class="menu menu-rigth menuDad"><a href="cerrar.php">Salir</a></li>

                        <li class="menu menu-rigth menuDad" ><a href="carrito.php" ><?php echo $_SESSION['usuario']?><span class="icon-basket"></span></a>
       <?php endif; ?>
                     </form>
                </li>
               </ul>
           </nav>
       </<heade>

              <div id="slides">
                  <ul class="slides-container">
                      <li><img class="img-slider" src="imagenes/courrusel1.jpg" alt=""></li>
                      <li><img class="img-slider" src="imagenes/courrusel3.jpg" alt=""></li>
                      <li><img class="img-slider" src="imagenes/courrusel4.jpg" alt=""></li>
                      <li><img class="img-slider" src="imagenes/courrusel6.jpg" alt=""></li>
                  </ul>
                  <nav class="slides-navigation">
                        <a href="#" class="next"> &#62;</a>
                        <a href="#" class="prev"> &#60;</a>
                  </nav>
              </div>


         <div class="contenedor">

              <h1 class="titulo">Bienvenido a Calvin Klein</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi placeat, alias tempora sint cupiditate dolores nihil necessitatibus quos tenetur, ab reiciendis, iure soluta dolore harum deserunt ad maxime vero perferendis?</p>
        </div>
        <section class="container">
              <figure class="img-container">
                  <a href="#!"></a><img class="uresponsiv"src="imagenes/menu1.jpg" alt="">
              </figure>
              <figure class="img-container">
                  <a href="#!"></a><img class="uresponsiv"src="imagenes/menu2.jpg" alt="">
              </figure>
              <figure class="img-container">
                  <a href="#!"></a><img class="uresponsiv"src="imagenes/images%20(6).jpg" alt="">
              </figure>
        </section>
        <footer>
         <div class="sociles">
             <a href="https://www.facebook.com/calvinkleinmexico/?ref=ts&fref=ts" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
             <a href="https://www.youtube.com/user/calvinklein" target="_blank"><i class="fa fa-youtube fa-2x"></i></a>
             <a href="https://twitter.com/calvinklein?lang=es" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
         </div>
          <small>
             <i class="fa fa-copyright fa-2x copy"></i>
              <p>@curiel todos los derechos reservados 2016</p>
          </small>
        </footer>

        <script src="js/jquery.min.js"></script>
        <script src="js/responsive.min.js"></script>
        <script src="js/jquery.superslides.min.js"></script>
    </body>
</html>
