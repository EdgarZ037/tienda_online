<?php session_start();

error_reporting(0);

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $usuario = filter_var(strtolower($_POST['usuario']), FILTER_SANITIZE_STRING);
    $correo = filter_var($_POST['correo'], FILTER_SANITIZE_EMAIL);
    $paswd = $_POST['passwd'];
    $paswd2 = $_POST['passwd2'];

    $enviado = '';
    $errores = '';

    if(empty($usuario) or empty($paswd) or empty($paswd2)){
        $errores .='Llena los campos correctamente'. "</br>";
    }else{

    require 'conexion.php';

    $statement = $conexion -> prepare('SELECT nombre FROM usuarios WHERE nombre = ? LIMIT 1');
    $statement -> bind_param('s', $usuario);
    $statement -> execute();
    $result = $statement -> fetch();

      print_r($result);
        if($result === true){
            $errores .= 'El nombre de usuario ya existe'. "</br>";
        }

        $paswd = hash('sha512', $paswd);
        $paswd2 = hash('sha512', $paswd2);
        //print_r($paswd);
        if($paswd !== $paswd2 ){
            $errores .='Las contraseñas no son iguales';
        }
    }

    if($errores == ''){

    $statement = $conexion -> prepare("INSERT INTO usuarios (nombre, passwd, correo) VALUES (?, ?, ?)");
    // echo '<pre>', print_r($statement), '</pre';

   $statement -> bind_param('sss', $usuario, $paswd, $correo);
   $statement -> execute();

        $enviado .= 'Se ha registrado exitosamente';
    }
}
require 'views/registro.view.php';

?>
